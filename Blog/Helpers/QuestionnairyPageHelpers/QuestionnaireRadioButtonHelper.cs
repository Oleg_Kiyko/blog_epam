﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Helpers.QuestionnairyHelpers
{

    public static class QuestionnaireRadioHelper
    {
        /// <summary>
        /// Create the radio button with specified values
        /// </summary>
        /// <param name="helper">Basic helpers</param>
        /// <param name="name">Element name</param>
        /// <param name="isCheked">Is element choosed</param>
        /// <param name="value">Element value</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString BlogRadioButtonHelper(this HtmlHelper helper, string name, 
            bool isCheked, string value)
        {
            TagBuilder newP = new TagBuilder("p");
            TagBuilder newRadio = new TagBuilder("input");
            newRadio.MergeAttribute("type", "radio");
            newRadio.MergeAttribute("name", name);
            newRadio.MergeAttribute("value", value);
            if (isCheked)
            {
                newRadio.MergeAttribute("checked", "true");
            }
            newP.InnerHtml += newRadio.ToString();
            newP.InnerHtml += " " + value;
            return new MvcHtmlString(newP.ToString());
        }
    }
}