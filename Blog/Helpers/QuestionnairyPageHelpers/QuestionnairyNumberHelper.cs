﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Helpers.QuestionnairyHelpers
{
    public static class QuestionnairyNumberHelper
    {
        /// <summary>
        /// Creates the element number
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="initValue">Start value</param>
        /// <param name="minValue">Min value</param>
        /// <param name="maxValue">Max value</param>
        /// <param name="name">Element name</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString NumberHelper(this HtmlHelper helper, string initValue,
            string minValue, string maxValue,
            string name)
        {
            TagBuilder newNumber = new TagBuilder("input");
            newNumber.MergeAttribute("type", "number");
            newNumber.MergeAttribute("name", name);
            newNumber.MergeAttribute("value", initValue);
            newNumber.MergeAttribute("min", minValue);
            newNumber.MergeAttribute("max", maxValue);
            return new MvcHtmlString(newNumber.ToString());
        }
    }
}