﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Helpers.QuestionnairyHelpers
{
    public static class QuestionnaireCheckBoxHelper
    {
        /// <summary>
        /// Creates checkbox with specified name and have value = true
        /// </summary>
        /// <param name="helper">Basic helpers</param>
        /// <param name="name">Name for new element</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString CheckBoxHelper(this HtmlHelper helper, string name, string value)
        {
            TagBuilder newP = new TagBuilder("p");
            TagBuilder newCheckBox = new TagBuilder("input");
            newCheckBox.MergeAttribute("type", "checkbox");
            newCheckBox.MergeAttribute("name", name);
            newCheckBox.MergeAttribute("value", "true");
            newP.InnerHtml += newCheckBox.ToString();
            newP.InnerHtml += " " + value;
            return new MvcHtmlString(newP.ToString());
        }
    }
}