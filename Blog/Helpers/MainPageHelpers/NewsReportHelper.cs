﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogData.News.Models;

namespace Blog.Helpers.MainPageHelpers
{
    public static class NewsReportHelper
    {
        /// <summary>
        /// Creates the container with news reports
        /// </summary>
        /// <param name="html">Basic helpers</param>
        /// <param name="reports">News reports which are needed to add in container</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString Report(this HtmlHelper html, IQueryable<NewsReport> reports)
        {
            //Container for news reports
            TagBuilder newContainer = new TagBuilder("div");
            newContainer.AddCssClass("news-container");
            foreach (var x in reports)
            {
                //Container for new report
                TagBuilder newNews = new TagBuilder("div");
                //News report header
                newNews.AddCssClass("news");
                TagBuilder newNewsHeader = new TagBuilder("div");
                newNewsHeader.AddCssClass("news-header");
                //News report name
                TagBuilder newReportName = new TagBuilder("h2");
                newReportName.SetInnerText(x.ReportHeader);
                //News report date
                TagBuilder newData = new TagBuilder("span");
                newData.SetInnerText(x.Date);
                newNewsHeader.InnerHtml += newReportName.ToString();
                newNewsHeader.InnerHtml += newData.ToString();
                //New report content
                TagBuilder newReportContent = new TagBuilder("div");
                newReportContent.AddCssClass("news-content");
                TagBuilder newPar = new TagBuilder("p");
                newPar.SetInnerText(CutNewsContent(x.ReportContent));
                newReportContent.InnerHtml += newPar.ToString();
                //Add header and content
                newNews.InnerHtml += newNewsHeader.ToString();
                newNews.InnerHtml += newReportContent.ToString();
                //Link to full article
                TagBuilder newLink = new TagBuilder("a");
                newLink.MergeAttribute("href", "NewsReportPage/Index/" + x.NewsReportId);
                newLink.AddCssClass("news-link");
                newLink.SetInnerText("Подробнее...");
                newNews.InnerHtml += newLink.ToString();
                //Add report to reports content
                newContainer.InnerHtml += newNews.ToString();
            }

            return new MvcHtmlString(newContainer.ToString());
        }

        /// <summary>
        /// Cuts report content to 200 symbols
        /// </summary>
        /// <param name="content">Report tu cut</param>
        /// <returns>Cuted report</returns>
        private static string CutNewsContent(string content)
        {
            if (content.Length > 200)
            {
                int lastIndexOfPoint = content.LastIndexOf('.', 199, 200);
                return content.Substring(0, lastIndexOfPoint==-1?200:lastIndexOfPoint) + " ...";
            }
            else
            {
                return content;
            }
        }
    }
}