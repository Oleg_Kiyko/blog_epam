﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Helpers
{

    public static class QuestionnaireRadioHelper
    {
        /// <summary>
        /// Создает элемент radio с заданными значениями
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="name">Имя элемента</param>
        /// <param name="isCheked">Выбран ли элемент</param>
        /// <param name="value">Значение элемента</param>
        /// <returns></returns>
        public static MvcHtmlString BlogRadioButtonHelper(this HtmlHelper helper, string name, 
            bool isCheked, string value)
        {
            TagBuilder newP = new TagBuilder("p");
            TagBuilder newRadio = new TagBuilder("input");
            newRadio.MergeAttribute("type", "radio");
            newRadio.MergeAttribute("name", name);
            newRadio.MergeAttribute("value", value);
            if (isCheked)
            {
                newRadio.MergeAttribute("checked", "true");
            }
            newP.InnerHtml += newRadio.ToString();
            newP.InnerHtml += " " + value;
            return new MvcHtmlString(newP.ToString());
        }
    }
}