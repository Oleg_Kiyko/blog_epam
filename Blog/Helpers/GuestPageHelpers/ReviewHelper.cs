﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogData.Reviews.Models;

namespace Blog.Helpers.GuestPageHelpers
{
    public static class ReviewHelper
    {
        /// <summary>
        /// Create the conainer with reviews
        /// </summary>
        /// <param name="helper">Basic helpers</param>
        /// <param name="reviews">Reviews collection which are needed to add to container</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString Review(this HtmlHelper helper, IQueryable<Review> reviews)
        {
            //Container for reviews
            TagBuilder newReviewContainer = new TagBuilder("div");
            newReviewContainer.AddCssClass("reviews-container");

            foreach (var x in reviews)
            {
                //Container for review
                TagBuilder newReview = new TagBuilder("div");
                newReview.AddCssClass("review");
                //Container for review header
                TagBuilder newReviewHeader = new TagBuilder("div");
                newReviewHeader.AddCssClass("review-header");
                //User avatar
                TagBuilder newAvatar = new TagBuilder("img");
                newAvatar.MergeAttribute("src", "../../Images/user-avatar.png");
                newAvatar.MergeAttribute("alt", "user-avatar");
                //Use name
                TagBuilder newUserName = new TagBuilder("h2");
                newUserName.SetInnerText(x.Author);
                //Review date
                TagBuilder newReviewDate = new TagBuilder("span");
                newReviewDate.SetInnerText(x.Date);
                newReviewHeader.InnerHtml += newAvatar.ToString();
                newReviewHeader.InnerHtml += newUserName.ToString();
                newReviewHeader.InnerHtml += newReviewDate.ToString();
                //Review content
                TagBuilder newReviewContent = new TagBuilder("p");
                newReviewContent.SetInnerText(x.ReviewContent);
                newReview.InnerHtml += newReviewHeader.ToString();
                newReview.InnerHtml += newReviewContent.ToString();
                newReviewContainer.InnerHtml += newReview.ToString();
            }
            return new MvcHtmlString(newReviewContainer.ToString());
        }

        /// <summary>
        /// Create the conainer with reviews
        /// </summary>
        /// <param name="helper">Basic helpers</param>
        /// <param name="reviews">Reviews collection which are needed to add to container</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString Review(this HtmlHelper helper, List<Review> reviews)
        {
            //Container for reviews
            TagBuilder newReviewContainer = new TagBuilder("div");
            newReviewContainer.AddCssClass("reviews-container");

            foreach (var x in reviews)
            {
                //Container for review
                TagBuilder newReview = new TagBuilder("div");
                newReview.AddCssClass("review");
                //Container for review header
                TagBuilder newReviewHeader = new TagBuilder("div");
                newReviewHeader.AddCssClass("review-header");
                //User avatar
                TagBuilder newAvatar = new TagBuilder("img");
                newAvatar.MergeAttribute("src", "../../Images/user-avatar.png");
                newAvatar.MergeAttribute("alt", "user-avatar");
                //Use name
                TagBuilder newUserName = new TagBuilder("h2");
                newUserName.SetInnerText(x.Author);
                //Review date
                TagBuilder newReviewDate = new TagBuilder("span");
                newReviewDate.SetInnerText(x.Date);
                newReviewHeader.InnerHtml += newAvatar.ToString();
                newReviewHeader.InnerHtml += newUserName.ToString();
                newReviewHeader.InnerHtml += newReviewDate.ToString();
                //Review content
                TagBuilder newReviewContent = new TagBuilder("p");
                newReviewContent.SetInnerText(x.ReviewContent);
                newReview.InnerHtml += newReviewHeader.ToString();
                newReview.InnerHtml += newReviewContent.ToString();
                newReviewContainer.InnerHtml += newReview.ToString();
            }
            return new MvcHtmlString(newReviewContainer.ToString());
        }
    }
}