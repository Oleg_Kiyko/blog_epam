﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Helpers.BasicLayoutHelpers
{
    public static class ActionListHelper
    {
        /// <summary>
        /// Create the list of links
        /// </summary>
        /// <param name="helper"></param>
        /// <param name="links">Links which are needed to add in list</param>
        /// <returns>Created element</returns>
        public static MvcHtmlString ActionList(this HtmlHelper helper, MvcHtmlString[] links)
        {
            TagBuilder newList = new TagBuilder("ul");
            foreach (var x in links)
            {
                TagBuilder newLi = new TagBuilder("li");
                newLi.InnerHtml += x.ToHtmlString();
                newList.InnerHtml += newLi.ToString();
            }
            return new MvcHtmlString(newList.ToString());
        }
    }
}