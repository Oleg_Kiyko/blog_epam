﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;

using BlogData.Repository.Abstract;
using BlogData.Repository.NewsRepository;
using BlogData.News.Models;
using BlogData.Reviews.Models;

namespace Blog.Controllers
{
    public class NewsReportPageController : Controller
    {
        /// <summary>
        /// Contains the operations which process data about news reports and news report's comments which are located in database
        /// </summary>
        private INewsRepository _repository = new NewsRepository(ConfigurationManager.ConnectionStrings["BlogDataContext"].ConnectionString);

        /// <summary>
        /// Loads news reports with certain id and send to view
        /// </summary>
        /// <param name="id">News report's id</param>
        /// <returns></returns>
        public ActionResult Index(int id)
        {
            Session["id"] = id;
            return View(_repository.GetById(id));
        }

        /// <summary>
        /// Adds new comment to news report
        /// </summary>
        /// <param name="newReview">Comment to addition</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult AddedNewComment(Review newReview)
        {
            int currentId = (int)Session["id"];
            var x = _repository.GetById(currentId);
            newReview.Date = DateTime.Now.ToString();
            newReview.NewsReport = x;
            x.ReportReviews.Add(newReview);
            _repository.Update(x);
            return RedirectToRoute(new { 
                controller = "NewsReportPage",
                action = "Index",
                id = currentId
            });
        }
    }
}
