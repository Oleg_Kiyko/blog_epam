﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Models;

namespace Blog.Controllers
{
    public class QuestionnaireController : Controller
    {
        /// <summary>
        /// Shows the questionnairy page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Processes data from quastionnairy form and shows information about user's questionnairy
        /// </summary>
        /// <param name="data">Data from questionnairy form</param>
        /// <returns></returns>
        public ActionResult AddQuestionnairy(QuestionnaireData data)
        {
            if (HttpContext.Request.RequestType == "POST")
            {
                ViewBag.Name = data.Name;
                return View();
            }

            return View("~/Views/Questionnairy/Index.cshtml");
        }
    }
}
