﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BlogData.Reviews.Models;
using BlogData.Repository;
using BlogData.Repository.ReviewsRepository;
using BlogData.Repository.Abstract;

namespace Blog.Controllers
{

    public class GuestPageController : Controller
    {
        /// <summary>
        /// Contains an operations which process data about users reviews which are located in database 
        /// </summary>
        private IReviewsRepository _repository = new ReviewsRepository(ConfigurationManager.ConnectionStrings["BlogDataContext"].ConnectionString);

        /// <summary>
        /// Shows guest page and sends users reviews from database to view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(_repository.GetGuestPageReviews);
        }

        /// <summary>
        /// Processes data about added review and add new review to database
        /// After processing data, shows page with information about user's review 
        /// </summary>
        /// <param name="review">Added review</param>
        /// <returns></returns>
        public ActionResult AddReview(Review review)
        {
            if (HttpContext.Request.HttpMethod == "POST")
            {
                if (ModelState.IsValid)
                {
                    ViewBag.Answer = "Ваш комментарий будет рассмотрен администратором сайта, ожидайте.";
                    _repository.Add(review);
                    return View();
                }
            }
            return View("~/Views/GuestPage/Index.cshtml", _repository.GetAll);
        }
    }
}