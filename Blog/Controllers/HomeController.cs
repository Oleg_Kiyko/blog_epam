﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using BlogData.Repository;
using BlogData.Repository.Abstract;
using BlogData.Repository.NewsRepository;

namespace Blog.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        ///  Contains an operations which process data about news reports which are located in database
        /// </summary>
        private INewsRepository _repository = new NewsRepository(ConfigurationManager.ConnectionStrings["BlogDataContext"].ConnectionString);

        /// <summary>
        /// Shows home page and sends news reports from database to view
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(_repository.GetAll);
        }
    }
}
