﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using BlogData.Repository.Abstract;
using BlogData.Repository.VotingsRepositories;
using BlogData.Votings;

namespace Blog.Controllers
{
    public class VotingsController : Controller
    {
        /// <summary>
        /// Conatains voting information
        /// </summary>
        IVotingFormCatsRepository _repository = new VotingAboutCatsRepository(ConfigurationManager.ConnectionStrings["BlogDataContext"].ConnectionString);

        /// <summary>
        /// Shows voting is user didn't vote
        /// Shows voting result if user has already voted
        /// </summary>
        /// <returns></returns>
        public ActionResult Voting()
        {
            bool? isAlreadyVoted = Convert.ToBoolean(Session["IsVoted"]);
            var voting = _repository.GetVoting;
            Session["Voting"] = voting;

            if (isAlreadyVoted == true)
            {
                bool isLike = Convert.ToBoolean(Session["isLike"]);
                ViewBag.Like = isLike;
                return PartialView("~/Views/Votings/VotingResult.cshtml", voting);
            }
            else
            {
                return PartialView("~/Views/Votings/Voting.cshtml");
            }

        }

        /// <summary>
        /// Adds new vote to voting repository
        /// After addition shows start page
        /// </summary>
        /// <param name="votingVariant">User's vote</param>
        /// <returns>Redirect to start page</returns>
        [HttpGet]
        public ActionResult AddVote(bool votingVariant)
        {
            VotingAboutCats voting = (VotingAboutCats)Session["Voting"];

            if (votingVariant)
            {
                Session["isLike"] = true;
                voting.PeopleWhoLoveCatsCount++;
            }
            else
            {
                Session["isLike"] = false;
                voting.PeopleWhoDontLoveCatsCount++;
            }

            _repository.Update(voting);
            Session["IsVoted"] = true;
            return RedirectToAction("Index", "Home");
        }
    }
}
