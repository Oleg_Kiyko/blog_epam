﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using BlogData.News.Models;
using BlogData.Repository.Context;
using BlogData.Repository.Abstract;
using BlogData.Repository.NewsRepository;

namespace Blog.Controllers
{
    public class NewsReportOfferController : Controller
    {
        /// <summary>
        /// Contains the operations which process data about news reports which are located in database
        /// </summary>
        private INewsRepository _repository = new NewsRepository(ConfigurationManager.ConnectionStrings["BlogDataContext"].ConnectionString);
        
        /// <summary>
        /// Shows the news offer page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Processes data about offered news report and adds new news report to database
        /// After that shows result about addition new news report
        /// </summary>
        /// <param name="newReport">News report new addition</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddNewsReport(NewsReport newReport)
        {
            if (ModelState.IsValid)
            {
                _repository.Add(newReport);
                ViewBag.Name = newReport.Author;
                return View();
            }
            else
            {
                return View("~/Views/NewsReportOffer/Index.cshtml");
            }
        }
    }
}
