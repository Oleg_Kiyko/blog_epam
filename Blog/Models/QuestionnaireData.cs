﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class QuestionnaireData
    {
        /// <summary>
        /// User name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User surname
        /// </summary>
        public string Surname { get; set; }

        /// <summary>
        /// User age
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// User gender
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// User country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Does user prefers news about football
        /// </summary>
        public bool PreferFootballNews { get; set; }

        /// <summary>
        /// Does user prefers news about politics
        /// </summary>
        public bool PreferPoliticianNews { get; set; }

        /// <summary>
        /// Does user 
        /// </summary>
        public bool PreferCultureNews { get; set; }

        /// <summary>
        /// Does user prefers news about IT
        /// </summary>
        public bool PreferItNews { get; set; }
    }
}