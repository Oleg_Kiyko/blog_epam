﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.News.Models;

namespace BlogData.Reviews.Models
{
    public class Review
    {
        /// <summary>
        /// Unique review identifier
        /// </summary>
        public int ReviewId { get; set; }

        /// <summary>
        /// Review's author
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage="Name must be longer than 2 and shorter then 20 symbols")]
        public string Author { get; set; }

        /// <summary>
        /// Review content
        /// </summary>
        [Required(ErrorMessage = "Review cannot be empty")]
        public string ReviewContent { get; set; }

        /// <summary>
        /// Review date
        /// </summary>
        public string Date { get; set; }

        public NewsReport NewsReport { get; set; }
    }
}
