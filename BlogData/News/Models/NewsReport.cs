﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.Reviews.Models;

namespace BlogData.News.Models
{
    public class NewsReport
    {
        /// <summary>
        /// Unique news report identifier
        /// </summary>
        public int NewsReportId { get; set; }

        /// <summary>
        /// News report author
        /// </summary>
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Name must be longer than 2 and shorter then 20 symbols")]
        public string Author { get; set; }

        /// <summary>
        /// News report author email
        /// </summary>
        [Required]
        public string AuthorEmail { get; set; }

        /// <summary>
        /// News report header
        /// </summary>
        [Required]
        [StringLength(100, MinimumLength=10, ErrorMessage="Header must be longer then 10 and shorter then 50 symbols")]
        public string ReportHeader { get; set; }

        /// <summary>
        /// News report content
        /// </summary>
        [Required(ErrorMessage="News cannot be empty")]
        public string ReportContent { get; set; }

        /// <summary>
        /// News report tegs
        /// </summary>
        public string ReportTags { get; set; }

        /// <summary>
        /// News report date
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// News report's reviews
        /// </summary>
        public List<Review> ReportReviews { get; set; }
    }
}
