﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogData.Votings
{
    public class VotingAboutCats
    {
        /// <summary>
        /// Voting id
        /// </summary>
        public int VotingAboutCatsId { get; set; }

        /// <summary>
        /// Count of people who like cats
        /// </summary>
        public int PeopleWhoLoveCatsCount { get; set; }

        /// <summary>
        /// Count of people who don't like cats
        /// </summary>
        public int PeopleWhoDontLoveCatsCount { get; set; }
    }
}
