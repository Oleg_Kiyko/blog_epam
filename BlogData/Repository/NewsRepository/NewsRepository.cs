﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.EntityModel;

using BlogData.News.Models;
using BlogData.Reviews.Models;
using BlogData.Repository.Abstract;
using BlogData.Repository.Context;

namespace BlogData.Repository.NewsRepository
{
    public class NewsRepository : INewsRepository
    {
        /// <summary>
        /// Blog's database
        /// </summary>
        private BlogDataContext _context;

        /// <summary>
        /// Initializes database
        /// </summary>
        /// <param name="connection">Connection string to database</param>
        public NewsRepository(string connection) 
        {
            _context = new BlogDataContext(connection);
        }

        /// <summary>
        /// Adds news report to database
        /// </summary>
        /// <param name="newReport">news report for addition</param>
        public void Add(NewsReport newReport)
        {
            newReport.Date = DateTime.Now.ToString();
            _context.NewsReports.Add(newReport);
            _context.SaveChanges();
        }

        /// <summary>
        /// Searches news report in database with certain id
        /// </summary>
        /// <param name="id">News report's id</param>
        /// <returns>Founded news report</returns>
        public NewsReport GetById(int id)
        {
            NewsReport report = _context.NewsReports.Where(x => x.NewsReportId == id).First();
            report.ReportReviews = _context.Reviews.Where(x => x.NewsReport.NewsReportId == report.NewsReportId).ToList();
            return report;
        }

        /// <summary>
        /// Updates certain new report
        /// </summary>
        /// <param name="newsReport">News report to update</param>
        public void Update(NewsReport newsReport)
        {
            _context.NewsReports.Attach(newsReport);
            _context.Entry(newsReport).State = EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Gets all news reports from database
        /// </summary>
        public IQueryable<NewsReport> GetAll { get { return _context.NewsReports; } }
    }
}
