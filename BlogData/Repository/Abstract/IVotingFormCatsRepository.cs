﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.Votings;

namespace BlogData.Repository.Abstract
{
    public interface IVotingFormCatsRepository
    {
        /// <summary>
        /// Add new voting statistics about cats to database
        /// </summary>
        /// <param name="voting">Voting form for addition</param>
        void Add(VotingAboutCats voting);

        /// <summary>
        /// Update certain voting statistics about cats
        /// </summary>
        /// <param name="voting">Voting form for update</param>
        void Update(VotingAboutCats voting);

        /// <summary>
        /// Get first voting statistics about cats
        /// </summary>
        VotingAboutCats GetVoting { get; }
    }
}
