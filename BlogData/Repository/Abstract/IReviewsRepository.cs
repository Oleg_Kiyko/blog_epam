﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.Reviews.Models;

namespace BlogData.Repository.Abstract
{
    public interface IReviewsRepository
    {
        /// <summary>
        /// Adds new review to database
        /// </summary>
        /// <param name="newReview">Review for addition</param>
        void Add(Review newReview);

        /// <summary>
        /// Gets review with certain id
        /// </summary>
        /// <param name="id">Review's id</param>
        /// <returns>Founded review</returns>
        Review GetById(int id);

        /// <summary>
        /// Gets all reviews from database
        /// </summary>
        IQueryable<Review> GetAll{ get; }

        /// <summary>
        /// Gets only guest page reviews
        /// </summary>
        IQueryable<Review> GetGuestPageReviews { get; }
    }
}
