﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.News.Models;


namespace BlogData.Repository.Abstract
{
    public interface INewsRepository
    {
        /// <summary>
        /// Adds news report to database
        /// </summary>
        /// <param name="newReport">news report for addition</param>
        void Add(NewsReport newReport);

        /// <summary>
        /// Searches news report in database with certain id
        /// </summary>
        /// <param name="id">News report's id</param>
        /// <returns>Founded news report</returns>
        NewsReport GetById(int id);


        /// <summary>
        /// Updates certain new report
        /// </summary>
        /// <param name="newsReport">News report to update</param>
        void Update(NewsReport newsReport);

        /// <summary>
        /// Gets all news reports from database
        /// </summary>
        IQueryable<NewsReport> GetAll { get; }
    }
}
