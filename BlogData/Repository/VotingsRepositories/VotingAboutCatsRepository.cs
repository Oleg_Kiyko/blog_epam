﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BlogData.Votings;
using BlogData.Repository.Abstract;
using BlogData.Repository.Context;

namespace BlogData.Repository.VotingsRepositories
{
    public class VotingAboutCatsRepository: IVotingFormCatsRepository
    {
        /// <summary>
        /// Blog's database
        /// </summary>
        private BlogDataContext _context;

        /// <summary>
        /// Initialize blog's database
        /// </summary>
        /// <param name="connection">Connection string to database</param>
        public VotingAboutCatsRepository(string connection)
        {
            _context = new BlogDataContext(connection);
        }

        /// <summary>
        /// Add new voting statistics about cats to database
        /// </summary>
        /// <param name="voting"></param>
        public void Add(VotingAboutCats voting)
        {
            _context.VotingAboutCats.Add(voting);
            _context.SaveChanges();
        }

        /// <summary>
        /// Udates current voting statistics about cats
        /// </summary>
        /// <param name="voting">voting statistics about cats to update</param>
        public void Update(VotingAboutCats voting)
        {
            _context.VotingAboutCats.Attach(voting);
            _context.Entry(voting).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();
        }

        /// <summary>
        /// Get first voting statistics about cats
        /// </summary>
        public VotingAboutCats GetVoting { get { return _context.VotingAboutCats.First(); } }
    }
}
