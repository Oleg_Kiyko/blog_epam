﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using BlogData.News.Models;
using BlogData.Reviews.Models;
using BlogData.Repository;
using BlogData.Votings;

namespace BlogData.Repository.Context
{
    public class BlogDataContextInitializer : CreateDatabaseIfNotExists<BlogDataContext>
    {

        /// <summary>
        /// Fills database if database doesn't exist
        /// </summary>
        /// <param name="context">Database</param>
        protected override void Seed(BlogDataContext context)
        {
            var reports = new List<NewsReport>
            {
                new NewsReport{
                    Author = "Alex",
                    AuthorEmail = "alex@mail.com",
                    ReportHeader = "Барселона выиграла очередной матч!",
                    ReportContent = "Барселона выиграла Тоттенхем со счетом 4:2...",
                    ReportTags = "#football#Barselona#Messi",
                    Date = DateTime.Now.ToString(),
                    ReportReviews = new List<Review>{
                        new Review{
                            Author = "Alex",
                            ReviewContent = "Барселона как всегда! Ничего нового!",
                            Date = DateTime.Now.ToString()
                        },
                        new Review{
                            Author="Oleg",
                            ReviewContent = "Гол Ракитича выше всех похвал!",
                            Date = DateTime.Now.ToString()
                        }
                    }
                },

                new NewsReport{
                    Author = "Alex",
                    AuthorEmail = "alex@gmail.com",
                    ReportHeader = "Ядро Linux обновили до версии 4.19",
                    ReportContent = "Нынешний глава разработки Linux Грег Кроа-Хартман (Greg Kroah-Hartman) представил релиз ядра Linux 4.19." + 
                                    " В этой версии добавлены нововведения, касающиеся как безопасности, так и нового аппаратного обеспечения.\n" +
                                    "\nУлучшение сети\n" +
                                    "Новая версия ядра поддерживает стандарт беспроводных сетей Wi-Fi 6 (802.11ax). Он использует диапазоны 2,4 ГГц и 5 ГГц, но также позволяет задействовать OFDMA и 1024-QAM модуляцию" +
                                    "В перспективе это позволит нарастить пропускную способность до 4 раз при росте мощности на 37 %. Прототипы устройств Wi-Fi 6 показали скорость до 11 Гбит/с.\n" +
                                    "Также в ядро добавили поддержу алгоритма управления сетевыми очередями CAKE (Common Applications Kept Enhanced)." +
                                    "Она уже есть в прошивке OpenWrt и максимизирует пропускную способность, а также снижает уровень задержек сигнала на домашних роутерах." +
                                    "Теперь такая возможность есть и на «самосборных» устройствах на Linux.\n" +
                                    "\nВиртуализация и безопасность\n" +
                                    "Для каталогов с битом sticky (например, /tmp) добавили защиту, которая позволяет удалять файл только администратору или владельцу. Это актуально для сетевых каталогов.\n" +
                                    "Другие улучшения касаются защиты от уязвимостей типа Spectre, Meltdown и других. Они затрагивают не только процессоры архитектуры x86-64, но также POWERPC. В частности, в новом ядре появилась защита от уязвимостей L1TF (L1 Terminal Fault) и SpectreRSB.\n" +
                                    "Для будущих процессоров Intel разработчик ядра внедрили поддержку расширенных инструкций IBRS (Enhanced Indirect Branch Restricted Speculation). Её намерены использовать  по умолчанию защитой от Spectre V2 вместо Retpoline, поскольку IBRS обеспечивает более высокую производительность.\n" +
                                    "Наконец, в ядре появился механизм, который определяет правила проверки компонентов ядра по цифровой подписи. Это даёт возможность проверить как всё ядро, так и его элементы.\n",
                    ReportTags = "#Linux#Ubuntu#OpenSource",
                    Date = DateTime.Now.ToString(),
                    ReportReviews = new List<Review>{
                        new Review{
                            Author = "Вася",
                            Date = DateTime.Now.ToString(),
                            ReviewContent = "Lunix forever, Windows must die"

                        }
                    }
                }
            };

            var reviews = new List<Review>
            {
                new Review{
                    Author = "Vasilii",
                    ReviewContent = "Сайт классный, лайк поставил!",
                    Date = DateTime.Now.ToString()
                }
            };

            var catVoting = new VotingAboutCats
            {
                PeopleWhoDontLoveCatsCount = 10,
                PeopleWhoLoveCatsCount = 159
            };

            reports.ForEach(x => context.NewsReports.Add(x));
            reviews.ForEach(x => context.Reviews.Add(x));
            context.VotingAboutCats.Add(catVoting);

            context.SaveChanges();
        }
    }
}
