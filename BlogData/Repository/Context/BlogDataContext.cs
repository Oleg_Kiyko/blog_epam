﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using BlogData.News.Models;
using BlogData.Reviews.Models;
using BlogData.Votings;

namespace BlogData.Repository.Context
{
    public class BlogDataContext : DbContext
    {
        /// <summary>
        /// Initializes database
        /// </summary>
        /// <param name="connectionString">Connection string to database</param>
        public BlogDataContext(string connectionString) : base(connectionString) { }

        /// <summary>
        /// Fill database if database doesn't exist
        /// </summary>
        static BlogDataContext()
        {
            Database.SetInitializer(new BlogDataContextInitializer());
        }

        /// <summary>
        /// Table which contains users reviews
        /// </summary>
        public IDbSet<Review> Reviews { get; set; }

        /// <summary>
        /// Table which contains new reports
        /// </summary>
        public IDbSet<NewsReport> NewsReports { get; set; }

        /// <summary>
        /// Table which contains voting statistics about cats
        /// </summary>
        public IDbSet<VotingAboutCats> VotingAboutCats { get; set; }
    }
}
