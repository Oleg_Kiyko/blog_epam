﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogData.Reviews.Models;
using BlogData.Repository.Abstract;
using BlogData.Repository.Context;

namespace BlogData.Repository.ReviewsRepository
{
    public class ReviewsRepository : IReviewsRepository
    {
        /// <summary>
        /// Blog's database
        /// </summary>
        private BlogDataContext _context;

        /// <summary>
        /// Initializes database
        /// </summary>
        /// <param name="connection">Connection string to database</param>
        public ReviewsRepository(string connection)
        {
            _context = new BlogDataContext(connection);
        }

        /// <summary>
        /// Adds new review to database
        /// </summary>
        /// <param name="newReview">Review for addition</param>
        public void Add(Review newReview)
        {
            newReview.Date = DateTime.Now.ToString();
            _context.Reviews.Add(newReview);
            _context.SaveChanges();
        }

        /// <summary>
        /// Gets review with certain id
        /// </summary>
        /// <param name="id">Review's id</param>
        /// <returns>Founded review</returns>
        public Review GetById(int id)
        {
            return _context.Reviews.First(x => x.ReviewId == id);
        }

        /// <summary>
        /// Gets all reviews from database
        /// </summary>
        public IQueryable<Review> GetGuestPageReviews { get { return _context.Reviews.Where(x => x.NewsReport == null).Select(x => x); } }

        /// <summary>
        /// Gets only guest page reviews
        /// </summary>
        public IQueryable<Review> GetAll { get { return _context.Reviews; } }
    }
}
